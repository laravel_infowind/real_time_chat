<html>
  
<head>
	<title>Socket Chat</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
	<link rel="stylesheet" href="./css/toastr.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./lib/css/emoji.css" rel="stylesheet">
	 <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="./js/toastr.js"></script>
	<script src="http://localhost:8080/socket.io/socket.io.js" > </script>
	<script>
	const socket = io('http://localhost:8080')
	const baseUrl = "http://localhost:8080/api/";
	var BASEURL = "http://localhost/node_front/";
	</script> 
</head>
<!--Coded with love by Mutiullah Samim-->
<body>
<div class="container">
	<div class="container h-100">
		<div class="d-flex justify-content-center h-100">
		
			<div class="user_card">
			     <div>
				 <h3>Signup</h3>
				 </div>
				<div class="d-flex justify-content-center form_container">
					<form id="register_form">
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class="far fa-envelope"></i></span>
							</div>
							<input type="text" id="email" name="email" class="form-control input_user" value="" placeholder="email">
						</div>
						
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
							<input type="text"  id="name" name="name" class="form-control input_pass" value="" placeholder="name">
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<input type="password"  id="password" name="password" class="form-control input_pass" value="" placeholder="password">
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-file-image"></i></span>
							</div>
							<input type="file"  id="profile" name="profile" class="form-control input_pass" value="">
						</div>
							<div class="d-flex justify-content-center mt-3 login_container">
				 	<button type="button" name="button" onclick="register()" class="btn login_btn">Register</button>
				   </div>
					</form>
				</div>
				<div class="mt-4">
					<div class="d-flex justify-content-center links">
						Don't have an account? <a href="index.php" class="ml-2">login</a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
<script>

function register(){
	
	        var data = new FormData();
		 	data.append('email', $('#email').val());
		    data.append('name', $('#name').val());
		    data.append('password', $('#password').val());
			data.append('profile',$('#profile')[0].files[0]);
		 $.ajax({
            url:baseUrl+"auth/register" ,
			cache: false,
		    contentType: false,
		    enctype: 'multipart/form-data',
		    processData: false,
			data:data,
            method: 'post',
            dataType: 'JSON',
            success:function(res)
            {
				if(result.success){
					toastr.success('Registration successfully.');
					setTimeout(function(){
					 window.location = "<?php echo 'index.php' ?>"	
					},1000);
				}else{
					toastr.error(result.message);
				}
		    },
			error: function (error) {
				toastr.error(error.responseJSON.message);
			}
            
        });
}

</script>
<style>
	/* Coded with love by Mutiullah Samim */
		body,
		html {
			margin: 0;
			padding: 0;
			height: 100%;
			background: #60a3bc !important;
		}
		.user_card {
			height: 400px;
			width: 350px;
			margin-top: auto;
			margin-bottom: auto;
			background: #f39c12;
			position: relative;
			display: flex;
			justify-content: center;
			flex-direction: column;
			padding: 10px;
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			border-radius: 5px;

		}
		.brand_logo_container {
			position: absolute;
			height: 170px;
			width: 170px;
			top: -75px;
			border-radius: 50%;
			background: #60a3bc;
			padding: 10px;
			text-align: center;
		}
		.brand_logo {
			height: 150px;
			width: 150px;
			border-radius: 50%;
			border: 2px solid white;
		}
		.form_container {
			margin-top: 12px;
		}
		.login_btn {
			width: 100%;
			background: #c0392b !important;
			color: white !important;
		}
		.login_btn:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.login_container {
			padding: 0 2rem;
		}
		.input-group-text {
			background: #c0392b !important;
			color: white !important;
			border: 0 !important;
			border-radius: 0.25rem 0 0 0.25rem !important;
		}
		.input_user,
		.input_pass:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.custom-checkbox .custom-control-input:checked~.custom-control-label::before {
			background-color: #c0392b !important;
		}
		h3 {
       margin-left: 110px;
        }
</style>
<?php 
include("./common-page/footer.php");
?>