const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var passport   = require('passport');
const fileupload = require('express-fileupload')

const app = express();
const server = require('http').Server(app)
const io = require('socket.io')(server)


var corsOptions = {
  origin: "http://localhost:8080"
};
// socket code --------------------------------------------

 const rooms = {}
 
io.on('connection', socket => {
// join room --------------------------------------------
   socket.on('new-user', (roomArray, userId) => {
	   console.log(roomArray)
	   roomArray.forEach(room => {
				if (typeof rooms[room] == 'undefined'){
		         rooms[room] = { users: {} } 
				}else{
					socket.join(rooms)
				}
				rooms[room].users[socket.id] = userId
			});
	console.log(rooms);
  })
  
  // send message to connected  user--------------------------
  socket.on('send-chat-message', (room, message) => {
        socket.broadcast.emit('chat-message', { message: message })
  })
  
  
  socket.on('update-message', (data) => {
        socket.broadcast.emit('updated-message', data)
  })
  
  socket.on('delete_message', (data) => {
        socket.broadcast.emit('remove-message', data)
  })
  
  socket.on('leave-from-group', (data) => {
        socket.broadcast.emit('removed-from-group', data)
  })
  
  socket.on('add-into-group', (data) => {
        socket.broadcast.emit('added-into-group', data)
  })
  // typing acknoledment--------------------------
  socket.on('is_typing', (room, name,userId) => {
    socket.broadcast.emit('typing', { room: room, name: name,user_id:userId})
  })
  
  socket.on('is_online', (userId) => {
    socket.broadcast.emit('online', {user_id:userId})
  })

  socket.on('is_offline', (userId) => {
    socket.broadcast.emit('offline', {user_id:userId})
  })
  
  socket.on('message_receiving', (data) => {
    socket.broadcast.emit('message_received', data)
  })
  // disconnect from socket--------------------------
  socket.on('disconnect', () => {
    getUserRooms(socket).forEach(room => {
      socket.to(room).broadcast.emit('user-disconnected', rooms[room].users[socket.id])
      delete rooms[room].users[socket.id]
    })
  })
  
})

function getUserRooms(socket) {
  return Object.entries(rooms).reduce((names, [name, room]) => {
    if (room.users[socket.id] != null) names.push(name)
    return names
  }, [])
}

//app.use(cors(corsOptions));

// parse requests of content-type - application/json

require('./app/config/passport')(passport);
app.use(passport.initialize());
app.use(passport.session());
app.use(fileupload());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

 app.use(function (req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            res.setHeader('Access-Control-Allow-Credentials', true);
            next();
 }); 
const db = require("./app/models");

db.sequelize.sync();

app.use("/swagger", express.static(__dirname + "/swagger"));

require("./app/routes/chat.js")(app);
require("./app/routes/auth.js")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
