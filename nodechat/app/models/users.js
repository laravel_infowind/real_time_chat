var appRoot = require('app-root-path');
const config = require("../config/config.js");
module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    email: {
      type: Sequelize.STRING,
	     unique: true,
    },
    password: {
      type: Sequelize.STRING
    },
	profile: {
      type: Sequelize.STRING,
	    get () {
        return (this.getDataValue('profile'))?config.HOST +'/app/controllers/images/'+ this.getDataValue('profile'):config.HOST +'/app/controllers/images/user_default.png';
        }
    },
	name: {
      type: Sequelize.STRING
	 
    },
 
	is_online : {
		type: Sequelize.STRING
  },
  timezone:{
    type: Sequelize.STRING
  }
	
  },
  {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  },
   {
        underscored: true,
        classMethods: {
            getUserImage: (userid) => {
               User.findOne({where:{id:userid}}).then(user=>{
				 return  appRoot.path+'/app/controlles/images/'+user.profile;
			   });
            }
        }
    },
	{
		getterMethods: {
			fullName: function () {
				return 'yunush ansary';
			}
		}
	}
);

  return User;
};


