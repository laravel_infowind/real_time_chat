const config = require("../config/config.js");
module.exports = (sequelize, Sequelize) => {
  const Room = sequelize.define("room", {
    room_id: {
      type: Sequelize.STRING,
    },
	group_name: {
      type: Sequelize.STRING,
    },
	group_image: {
      type: Sequelize.STRING,
    },
	
	profile: {
      type: Sequelize.VIRTUAL,
	    get () {
			if(this.group_image){
			  profileUser = config.HOST +'/app/controllers/images/'+ this.getDataValue('group_image');
			}else{
              profileUser =  config.HOST +'/app/controllers/images/group_image.png';
			}
			return profileUser;
     }
    }
  },
  {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  }
);

  return Room;
};
