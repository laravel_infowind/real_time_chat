module.exports = (sequelize, Sequelize) => {
  const UserRoom = sequelize.define("user_room", {
    user_id : {
      type: Sequelize.INTEGER
	},
    room_id: {
      type: Sequelize.INTEGER
    },
    to_user:{
      type: Sequelize.STRING
    },
	type: {
      type: Sequelize.STRING
    }
	
  },
  {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  }
);

UserRoom.associate = models => {
   UserRoom.belongsTo(models.user,{
    foreignKey: 'user_id'
    });
   UserRoom.belongsTo(models.room,{
    foreignKey: 'room_id'
    });
 }
  return UserRoom;
};
