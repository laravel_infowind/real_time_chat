const db = require("../models");
const config = require("../config/config.js");
const User = db.users;
module.exports = (sequelize, Sequelize) => {
  const Message = sequelize.define("message", {
    room_id: {
      type: Sequelize.INTEGER
	},
    user_id: {
      type: Sequelize.INTEGER,
	
    },
	to_user: {
      type: Sequelize.INTEGER,
		
    },
	message: {
      type: Sequelize.STRING
    },
  deleted_by:{
    type: Sequelize.STRING
    },
	read_by:{
	 type: Sequelize.STRING
	},
	files:{
	 type: Sequelize.TEXT,
	}
  },

  {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  },
  
);
 Message.associate = models => {
     Message.belongsTo(models.user,{
    foreignKey: 'user_id'
    });
	Message.belongsTo(models.user,{
		as: 'toUser',
    foreignKey: 'to_user'
    });
   Message.belongsTo(models.room,{
    foreignKey: 'room_id'
    });
 }
  return Message;
};
