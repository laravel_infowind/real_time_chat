const db = require("../models");
const User = db.user;
const Room = db.room;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
var passport = require('passport');
var bcryptjs = require('bcryptjs');
var auth = require('../config/passport');
var appRoot = require('app-root-path');


// Create and Save a new user
exports.register = (req, res) => {


	var fileName = '';
	if (req.files) {
		const image = req.files.profile
		const path = __dirname + '/images/' + image.name
		image.mv(path, (error) => {
			if (error) {
				console.error(error)
				res.writeHead(500, {
					'Content-Type': 'application/json'
				})
				res.end(JSON.stringify({ status: 'error', message: error }))

			}
		})
		fileName = image.name
	}
	// Validate request
	if (!req.body.email) {
		res.status(400).send({
			message: "email can not be empty!"
		});
		return;
	}

	var salt = 10;
	var passwordHash;
	var userData = {};
	bcrypt.hash(req.body.password, salt, (err, encrypted) => {
		passwordHash = encrypted;
		userData = {
			email: req.body.email,
			password: passwordHash,
			name: req.body.name,
			profile: fileName
		};
		// Save Tutorial in the database
		User.create(userData)
			.then(data => {
				res.send({ 'success': true, 'data': data });
			})
			.catch(err => {
				res.status(500).send({
					'success': false,
					message:
						err.message || "Some error occurred while creating the Tutorial."
				});
			});
	})

};

exports.updateProfile = async function (req, res, next) {

	console.log(req.files);
	try {
		var fileName;
		if (req.files) {
			const image = req.files.profile
			const path = __dirname + '/images/' + image.name

			await image.mv(path, (error) => {
				if (error) {
					console.error(error)
					res.writeHead(500, {
						'Content-Type': 'application/json'
					})
					res.end(JSON.stringify({ status: 'error', message: error }))

				}
			})
			fileName = image.name;
		}

		if (req.body.type == 'private') {
			var userModel = await User.findOne({ where: { id: req.body.user_id } });
			if (userModel) {
				if (req.body.name) {
					userModel.name = req.body.name;
				}
				if (req.files) {
					userModel.profile = fileName;
				}
				userModel.save();
				res.send({ 'success': true, message: "profile updated successfully.", data: userModel });
			}
			res.send({ 'success': false, message: "record not found.", data: [] });
		} else {
			var roomModel = await Room.findOne({ where: { id: req.body.user_id } });
			if (roomModel) {
				if (req.body.name) {
					roomModel.group_name = req.body.name;
				}
				if (req.files) {
					roomModel.group_image = fileName;
				}
				roomModel.save();
				res.send({ 'success': true, message: "profile updated successfully.", data: roomModel });
			}
			res.send({ 'success': false, message: "record not found.", data: [] });

		}
	} catch (error) {
		res.send({ 'success': false, message: error.message, data: [] });
	}


}

exports.login = function (req, res, next) {

	console.log(req.body);
	User.findOne({ where: { email: req.body.email } }).then(data => {
		bcrypt.compare(req.body.password, data.password, function (err, isMatch) {
			if (err)
				console.log(err);
			if (isMatch) {
				var obj = Object.assign({}, data.get());
				let updateData = {
					is_online: 'online',
					timezone: req.body.timezone
				}
				User.update(updateData, { where: { email: data.email } });
				res.send({ 'success': true, 'data': obj });
			} else {
				res.send({ 'success': false, message: "invalid credential" });
			}
		});
	}).catch(err => {
		res.status(500).send({
			message:
				err.message || "Invalid credential."
		});
	});

}
exports.logout = function (req, res, next) {
	User.findOne({ where: { email: req.body.email } }).then(data => {
		let updateData = {
			is_online: 'offline'
		}
		User.update(updateData, { where: { email: data.email } });
		res.send({ 'success': true, 'message': 'Logout Successfully.' });
	}).catch(err => {
		res.status(500).send({
			message:
				err.message || "Invalid credential."
		});
	});

}
// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
	const title = req.query.title;
	var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

	Tutorial.findAll({ where: condition })
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			res.status(500).send({
				message:
					err.message || "Some error occurred while retrieving tutorials."
			});
		});
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
	const id = req.params.id;

	Tutorial.findByPk(id)
		.then(data => {
			res.send(data);
		})
		.catch(err => {
			res.status(500).send({
				message: "Error retrieving Tutorial with id=" + id
			});
		});
};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
	const id = req.params.id;

	Tutorial.update(req.body, {
		where: { id: id }
	})
		.then(num => {
			if (num == 1) {
				res.send({
					message: "Tutorial was updated successfully."
				});
			} else {
				res.send({
					message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found or req.body is empty!`
				});
			}
		})
		.catch(err => {
			res.status(500).send({
				message: "Error updating Tutorial with id=" + id
			});
		});
};

let getUserImage = (userid) => {
	User.findOne({ where: { id: userid } }).then(user => {
		var imagePath = appRoot.path + '/app/controlles/images/' + user.profile;
		return imagePath;
	});
}