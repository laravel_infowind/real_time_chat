var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })
const validationMiddleware = require('../middleware/validation-middleware');

module.exports = app => {
  const auth = require("../controllers/auth.js");

  var router = require("express").Router();
	
  // Create a new Tutorial
  router.post("/register",validationMiddleware.signup,auth.register);
  router.post("/login", validationMiddleware.login,auth.login);
  router.post("/update-profile",auth.updateProfile);
  router.post("/logout", auth.logout);
  app.use('/api/auth', router);
};
