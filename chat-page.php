<?php
   include("./common-page/header.php");
   ?>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
</head>
<div class="messaging">
   <div class="inbox_msg chat-page-main">
      <div class="inbox_people">
         <div class="chat-page-main-mesgs">
            <div class="row">
               <div class="col-sm-4">
                  <div class="left_chat_page">
                     <div class="left_heading">
                        <div class="recent_heading">
						    <h3>
                              <a href="user-list-page.php"><i class="fas fa-users"></i> <small>User List</small></a>
                           </h3>
                           <h3>
                              <span><i class="fas fa-comments"></i> <small>Conversation List</small></span>
                           </h3>
                           <h3>
                              <button type="button" value='' onclick="openModel()">
                              	<i class="fas fa-user-plus"></i><small>Creat Group</small>
                              </button> 
                           </h3>
                        </div>
                        <div class="srch_bar text-right">
                           <div class="stylish-input-group">
                              <input type="text" class="search-bar" onkeyup="getConnectedUserList()" placeholder="Search Username" />
                              <button style="display: none;" type="button" class="connected-user-search" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                              <span class="input-group-addon">
                              <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                              </span>
                           </div>
                        </div>
                     </div>
                     <div class="inbox_chat">
                     </div>
                  </div>
               </div>
               <div class="col-sm-8">
                  <div class="right_chat_page">
                     <div class='front-user-info' style='display:none'>
                        <div class="row">
                           <div class="col-sm-9">
                              <div class="chat_people">
                                 <div class="chat_img">
                                    <a href='javascript:void(0)' id='group_edit' style='display:none'><i onclick="editModel('group')" class="fas fa-pencil-alt"></i></a>
                                    <img id='to_user_profile' src="" alt="sunil">
                                    <span style="display:none" id="front_user_active" class="user-online"><i style="display:" class="fa fa-circle" aria-hidden="true"></i></span>
                                 </div>
                                 <div class="chat_ib">
                                    <div class="chat_dtls">
                                       <a href='javascript:void(0)' onclick='groupUsers()'>
                                          <h5 id='to_user_user_name'></h5>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="join-btn">
                                 <button style="display:none;float:right" onclick='joinGroupModel()' class='btn add-to-group' type="button"><i class="fa fa-user-plus" aria-hidden="true"></i><small>Join Users</small></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="mesgs">
                        <center><span id='current_date_message' style='display:none'></span></center>
                        <div class="msg_history">
                           <center>
                              <span id="message_history_loader">
                              <i class="fa-3x fas fa-spinner fa-spin"></i>
                              </span>
                           </center>
                        </div>
                        <span id="is_typing" style="display:none">is typing..........</span>
                        <div class="type_msg">
                           <form id="send_message_form">
                              <input id="message_emoji" type="hidden">
                              <div class="input-group">
                                 <input type='text' onkeyup="tellForTyping()" class="write_msg message_input" data-emojiable="true" placeholder="Type your message..." />
                                 <div class="input-group-append">
                                    <span onclick="sendMessage()" class="input-group-text send_btn"><i class="fa fa-paper-plane"></i></span>
                                 </div>
                                 <div class="input-group-append">
                                    <span class="input-group-text attach_btn"><i class="fa fa-paperclip"></i>
                                    <input id="images" id="uploadFile" type="file" name="chat_attachment" value="" multiple="">
                                    </span>
                                 </div>
                              </div>
                              <span id="message-error" class="invalid-feedback alert-danger" style="display: none;">The message field is required.</span>
                           </form>
                           <div id="image_preview"></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <input type="hidden" id="message_room" value="">
   <input type="hidden" id="room" value="">
   <input type="hidden" id="to_user" value="">
   <input type="hidden" id="login_user" value="">
   <input type="hidden" id="message_id" value="">
   <input type="hidden" id="last_sent_message_id" value="">
</div>
</div>
<div class="modal" id="modalSubscriptionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Create Group</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="group_form">
            <input id="initiate_message" type="hidden">
            <div class="modal-body mx-3">
               <div class="md-form mb-5">
                  <label>Group Name</label>
                  <input type="text" placeholder="Enter Group Name" name="group" id="group" class="form-control  require">
                  <span id='add_group_name' style='display:none' class="alert-danger">The field is required.</span>
               </div>
               <div class="md-form mb-4">
                  <label>Users</label>
                  <div id="body">
                     <select name="users[]" multiple="multiple" data-live-search="true" id="user-list" class="form-control select2 require">
                        <option value="">Select User </option>
                     </select>
                     <span id='add_group_user' style='display:none' class="alert-danger">The field is required.</span>
                  </div>
               </div>
            </div>
         </form>
         <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-indigo" onclick="saveGroup()">Save <i class="fas fa-paper-plane-o ml-1"></i></button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="joingroupmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Join Group</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="group_form">
            <div class="modal-body mx-3">
               <div class="md-form mb-4">
                  <label>Users</label>
                  <div id="body">
                     <select name="users[]" multiple="multiple" data-live-search="true" id="" class="user-list form-control select2">
                     </select>
                  </div>
               </div>
            </div>
         </form>
         <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-indigo" onclick="joinGroup()">Join <i class="fas fa-paper-plane-o ml-1"></i></button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="groupUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Group Users</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" id="goup-users">
         </div>
      </div>
   </div>
</div>
<?php
   $initiateRoomId = isset($_GET['room_id']) ? $_GET['room_id'] : '';
   
   
   
   $initiateRoom = isset($_GET['room']) ? $_GET['room'] : '';
   
   
   
   $toUser = isset($_GET['to_user']) ? $_GET['to_user'] : '';
   
   
   
   $chat_initiated = isset($_GET['chat_initiated']) ? $_GET['chat_initiated'] : '';
   
   
   
   
   
   ?>
<script>
   var firstRoomId;
   var firstRoom;
   var myRooms = [];
   var loginUserId;
   var firstToUserId;
   $(document).ready(function() {
   	$('.select2[multiple]').select2({
   		width: '100%',
   		closeOnSelect: false
   	})
   	loginUserId = localStorage.getItem('userId');
   	$('#login_user').val(loginUserId);
   	getAllRooms(loginUserId)
   	//setTimeout(function(){
   	getConnectedUserList(loginUserId);
   	//},500);
   	// setTimeout(function() {
   	// 	if (firstRoomId) {
   	// 		getUserMessage(firstToUserId, firstRoomId, firstRoom);
   	// 	}
   	// }, 3000);
   	if ('<?php echo $toUser ?>' && '<?php echo $chat_initiated ?>' == 'no') {
   		initiateChat()
   	}
   	joinRoom(loginUserId)
   	var profile = localStorage.getItem('loginUserProfile');
   	var name = localStorage.getItem('loginUserName');
   	$('#user_profile').attr("src", profile);
   	$('#user_name').html('<h5 >' + name + '</h5>')
   
   	$(".send_btn").css("pointer-events", "none");
   
   });
   
   function initiateChat() {
   	$('#initiate_message').val('Hi')
   	$('#message_room').val('<?php echo $initiateRoomId ?>')
   	myRooms.push('<?php echo $initiateRoomId ?>')
   	$('#to_user').val('<?php echo $toUser ?>')
   	console.log('me:' + $('#initiate_message').val())
   	sendMessage();
   }
   
   
   
   $("body").on('DOMSubtreeModified', ".emoji-wysiwyg-editor", function() {
   	tellForTyping()
   });
   
   function openModel() {
   	$.ajax({
   		url: baseUrl + "chat/user-list",
   		data: {
   			user_id: localStorage.getItem('userId'),
   			search: $('.search-bar').val()
   		},
   		method: 'get',
   		dataType: 'JSON',
   		success: function(result) {
   			$('#user-list').html('');
   			var str = "<option value=''>Select User</option>";
   			for (var i = 0; i < result.data.length; i++) {
   				str = str + "<option onclick='addUsers(" + this.value + ")' value='" + result.data[i].id + "'>" + result.data[i].name + "</option>"
   			}
   			$('#user-list').html(str);
   			$("<option >Select User </option>" ).insertAfter( "li .select2-search__field" );
   			$( "li .select2-search__field" ).remove();
   			$('#user-list').on('change',function(){
   				$('.select2-search option').hide();
   			});
   		}
   	});
   	$('#modalSubscriptionForm').show();
   }
   
   
   function joinGroupModel() {
   	$.ajax({
   		url: baseUrl + "chat/user-list",
   		data: {
   			user_id: localStorage.getItem('userId'),
   			search: $('.search-bar').val(),
   			room_id: $('#message_room').val()
   		},
   		method: 'get',
   		dataType: 'JSON',
   		success: function(result) {
   			$('#user-list').html('');
   			var str = "<option value=''>Select User</option>";
   			for (var i = 0; i < result.data.length; i++) {
   				str = str + "<option onclick='addUsers(" + this.value + ")' value='" + result.data[i].id + "'>" + result.data[i].name + "</option>"
   			}
   			$('.user-list').html(str);
   			$("<option >Select User </option>" ).insertAfter( "li .select2-search__field" );
   			$( "li .select2-search__field" ).remove();
   			$('.user-list').on('change',function(){
   				$('.select2-search option').hide();
   			});
   
   		}
   	});
   	$('#joingroupmodel').show();
   }
   
   function groupUsers() {
   	$.ajax({
   
   		url: baseUrl + "chat/group-users",
   
   		data: {
   			room_id: $('#message_room').val()
   		},
   
   		method: 'get',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   
   			$('#goup-users').html('');
   
   			var str = "";
   
   			for (var i = 0; i < result.data.length; i++) {
   
   				if (result.data[i].user_id != $('#login_user').val()) {
   
   					str = str + "<div  id='group_user_" + result.data[i].user_id + "' class='chat_list'><a href='javascript:void(0)' class='remove-url'>  <div class='chat_people'>" +
   
   						"<div class='chat_img'> <img src='" + result.data[i].user.profile + "' alt='sunil'> </div><div class='chat_ib'><h5>" + result.data[i].user.name + "<span >" +
   
   						"<i onclick='removeFromGroup(" + result.data[i].room_id + "," + result.data[i].user_id + ")' class='fas fa-trash ml-1'></i></span></h5></div></div></a></div>";
   
   				}
   
   			}
   
   			$('#goup-users').html(str);
   
   		}
   
   	});
   
   	$('#groupUserModal').show();
   
   }
   
   
   
   function removeFromGroup(roomId, userId)
   
   {
   
   	$.ajax({
   
   		url: baseUrl + "chat/remove-from-group",
   
   		data: {
   			room_id: roomId,
   			user_id: userId
   		},
   
   		method: 'post',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   
   			toastr.success(result.message);
   
   			$('#group_user_' + userId).remove();
   
   			socket.emit('leave-from-group', {
   				room_id: roomId,
   				user_id: $('#login_user').val()
   			})
   
   		}
   
   	});
   
   }
   
   var users = [];
   
   function addUsers(userId) {
   
   	users.push(userId)
   
   	console.log(users);
   
   }
   
   
   
   function saveGroup() {
   
   	var users = $('#user-list').val();
   
   	var flag = 0;
   	if ($('#group').val() == '') {
   		flag = 1;
   		$('#add_group_name').show();
   	} else {
   		$('#add_group_name').hide();
   	}
   	if (!users) {
   		flag = 1;
   		$('#add_group_user').show();
   	} else {
   		$('#add_group_user').hide();
   	}
   	if (!flag) {
   		var loginUserId = localStorage.getItem('userId');
   		users.push(loginUserId)
   		$.ajax({
   
   			url: baseUrl + "chat/create-room",
   
   			data: {
   				group: $('#group').val(),
   				"userId": JSON.stringify(users),
   				"type": "group"
   			},
   
   			method: 'post',
   
   			dataType: 'JSON',
   
   			success: function(result)
   
   			{
   				
   				if (!result) {
   					toastr.error('Some thing went wrong')
   				} else {
   
   					$('.emoji-wysiwyg-editor').html('Welcome to this Group')
   
   					$('#message_room').val(result.data.id)
   
   					myRooms.push(result.data.id)
   
   					$('#to_user').val(0)
   
   					sendMessage();
   
   					$('.msg_history').html('');
   
   					getConnectedUserList(loginUserId);
   
   					$('#modalSubscriptionForm').hide();
   
   					socket.emit('add-into-group', {
   						room_id: result.data.id,
   						user_id: $('#login_user').val()
   					})
   				}
   
   			}
   
   		})
   	}
   
   }
   
   
   
   function joinGroup() {
   
   	var users = $('.user-list').val();
   
   	$.ajax({
   
   		url: baseUrl + "chat/join-group",
   
   		data: {
   			room_id: $('#message_room').val(),
   			"users": JSON.stringify(users)
   		},
   
   		method: 'post',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   
   			$('.msg_history').html('');
   
   			getConnectedUserList(loginUserId);
   
   			$('#joingroupmodel').hide();
   
   			socket.emit('send-chat-message', $('#message_room').val(), result.data)
   
   			toastr.success('Group joined successfully.');
   
   			socket.emit('add-into-group', {
   				room_id: $('#message_room').val(),
   				user_id: $('#login_user').val()
   			})
   
   		}
   
   	})
   
   }
   
   
   
   $('.close').on('click', function() {
   
   	$('#modalSubscriptionForm').hide();
   
   	$('#joingroupmodel').hide();
   
   	$('#groupUserModal').hide();
   
   });
   
   
   
   $('#send_message_form').keypress(function(e) {
   
   	if (e.keyCode == 13 && !e.shiftKey)
   
   	{
   
   		e.preventDefault();
   
   		$('.write_msg').off('focus');
   		$('.write_msg').css({
   			'cursor': "pointer"
   		});
   		if ($('.send_btn').css('pointer-events') == 'auto') {
   			$('.send_btn').trigger('click');
   		}
   		//sendMessage()
   
   		return false;
   
   	}
   
   });
   
   $('#send_message_form').keydown(function(e) {
   	if (e.which == 38) {
   		editMessage($('#last_sent_message_id').val())
   	}
   });
   
   
   function joinRoom(loginUserId)
   
   {
   
   	$.ajax({
   
   		url: baseUrl + "chat/my-rooms",
   
   		data: {
   			"user_id": loginUserId
   		},
   
   		method: 'get',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   
   			socket.emit('new-user', result.data, loginUserId)
   
   		}
   
   	})
   
   }
   
   
   
   function getAllRooms(loginUserId)
   
   {
   
   	$.ajax({
   
   		url: baseUrl + "chat/room-array",
   
   		data: {
   			user_id: loginUserId
   		},
   
   		method: 'get',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   
   			myRooms = result.data;
   
   		}
   
   	})
   
   }
   var connectionPage = 1;
   var connectedTotalpage;
   var scroll = 'no';
   jQuery(function($) {
   	$('.inbox_chat').on('scroll', function() {
   		if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
   			if (connectionPage < connectedTotalpage) {
   				scroll = 'yes';
   				getConnectedUserList()
   			}
   		}
   	})
   });
   
   $('.connected-user-search').on('click',function(){
   	$('.search-bar').val('');
   	getConnectedUserList()
   });
   
   function getConnectedUserList(loginUserId, roomId)
   
   {
   
   	if($('.search-bar').val() == ''){
   		$('.connected-user-search').hide();
   	}else{
   		$('.connected-user-search').show();
   	}
   
   	if (scroll == 'yes' && (connectionPage < connectedTotalpage)) {
   		connectionPage++
   	}
   	$.ajax({
   
   		url: baseUrl + "chat/connected-user-list",
   
   		data: {
   			user_id: $('#login_user').val(),
   			search: $('.search-bar').val(),
   			page: connectionPage
   		},
   
   		method: 'get',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   
   			//$('.inbox_chat').html('');
   		
   			if (result.data.rows && result.data.rows.length > 0) {
   				$('#images').prop('disabled',false);
   				connectedTotalpage = result.data.totalPages;
   				firstRoomId = result.data.rows[0].room_id;
   
   				if ('<?php echo $toUser ?>') {
   
   					firstToUserId = '<?php echo $toUser ?>';
   
   				} else {
   
   					firstToUserId = (result.data.rows[0].toUser != null || result.data.rows[0].to_user != 0) ? ((result.data.rows[0].user.id == $('#login_user').val()) ? result.data.rows[0].toUser.id : result.data.rows[0].user.id) : 0;
   
   				}
   
   
   
   				$('#to_user').val(firstToUserId);
   
   				var str = '';
   
   
   
   				for (var i = 0; i < result.data.rows.length; i++) {
   
   					var room = "`" + result.data.rows[i].room.room_id + "`";
   
   					var toUserName = (result.data.rows[i].toUser != null || result.data.rows[i].to_user != 0) ? ((result.data.rows[i].user.id == $('#login_user').val()) ? result.data.rows[i].toUser.name : result.data.rows[i].user.name) : result.data.rows[i].room.group_name;
   
   					var toUserId = (result.data.rows[i].toUser != null || result.data.rows[i].to_user != 0) ? ((result.data.rows[i].user.id == $('#login_user').val()) ? result.data.rows[i].toUser.id : result.data.rows[i].user.id) : 0;
   
   					var toProfile = (result.data.rows[i].toUser != null || result.data.rows[i].to_user != 0) ? ((result.data.rows[i].user.id == $('#login_user').val()) ? result.data.rows[i].toUser.profile : result.data.rows[i].user.profile) : result.data.rows[i].room.profile;
   
   					firstRoom = result.data.rows[0].room.room_id;
   
   					var onlineClass = (result.data.rows[i].to_user != 0) ? 'user-online' : '';
   
   					var onlineDisplay = (result.data.rows[i].toUser && result.data.rows[i].toUser.is_online == 'online') ? '' : 'none';
   
   					var unraedCount = (result.data.rows[i].unreadCount != 0) ? "<span id='count_" + result.data.rows[i].room_id + "' class='uread-message-chat'>" + result.data.rows[i].unreadCount + "</span>" : ''
   
   					str = str + " <div id = 'connect_row_" + result.data.rows[i].room_id + "' class='chat_list'><input type='hidden' value='" + result.data.rows[i].to_user + "' id='to_user" + result.data.rows[i].room_id + "'>" +
   						"<a href='javascript:void(0)' class='remove-url' onclick = 'getUserMessage(" + toUserId + "," + result.data.rows[i].room_id + "," + room + ",`reset`)'>  " +
   						"<div class='chat_people'>" +
   						"<div class='chat_img'> <img src='" + toProfile + "' alt='sunil'> <span style='display:" + onlineDisplay + "' id='active" + result.data.rows[i].to_user + "' class=" + onlineClass + " ><i  class='fa fa-circle' aria-hidden='true'></i></span> </div>" +
   						"<div class='chat_ib'>" +
   						"<div class='chat_dtls'><h5>" + toUserName + "<span class='chat_date'>" + result.data.rows[i].created_at + "</span></h5>" +
   						"<p>" + unraedCount + result.data.rows[i].message + "</p>" +
   						"<span id='is_typing" + result.data.rows[i].room_id + "' style='display:none'>is typing..........</span>" +
   						"</div></div></div></a></div>";
   				}
   				if (scroll == 'no') {
   					$('.inbox_chat').html(str);
   				} else {
   					$('.inbox_chat').append(str);
   				}
   				if ($('.search-bar').val()=='') {
   				getUserMessage(firstToUserId, firstRoomId, firstRoom);
   				}
   				$('#connect_row_' + $('#message_room').val()).addClass('active_chat');
   
   			} else {
   				$('#message_history_loader').hide();
   				$('.inbox_chat').html('<center><h4>No User Found.</h4></center>');
   				//$('.msg_history').html('<center><h5>No Message Found.</h5></center>');
   				$('#images').prop('disabled',true);
   
   			}
   
   		}
   
   
   
   	});
   
   }
   
   function isVisible(row, container) {
   
   	var elementTop = $(row).offset().top,
   		elementHeight = $(row).height(),
   		containerTop = container.scrollTop(),
   		containerHeight = container.height();
   	return ((((elementTop - containerTop) + elementHeight) > 0) && ((elementTop - containerTop) < containerHeight));
   }
   
   var messagePage = 1;
   var messageTotalpage;
   var messageRoom;
   jQuery(function($) {
   	$('.msg_history').on('scroll', function() {
   
   		$('.msg_history div').each(function() {
   			if (isVisible($(this), $(window))) {
   				var eleClass = $(this).attr('class');
   				var elementId = $(this).attr('id');
   				if (elementId && eleClass == 'incoming_msg') {
   					var time = $('#' + elementId).find('.received_withd_msg').find('span').text();
   					time = time.split(',')[0];
   				} else if (elementId && eleClass == 'outgoing_msg') {
   					var time = $('#' + elementId).find('.sent_msg').find('.time_date').text();
   					time = time.split(',')[0];
   				}
   				
   				$('#current_date_message').text(time)
   				if ($(this).scrollTop() == 0) {
   					$('#current_date_message').show()
   					setTimeout(function() {
   						$('#current_date_message').hide()
   					}, 2000)
   				}
   
   			};
   		});
   		if ($(this).scrollTop() == 0) {
   			console.log('loadMore');
   			console.log(messagePage);
   			console.log(messageTotalpage);
   			if (messageTotalpage > messagePage) {
   				messagePage++;
   				if (messagePage != 1 && messagePage <= messageTotalpage) {
   					$('#load_more_loader').show();
   					$('#current_date_message').hide()
   					getUserMessage($('#to_user').val(), $('#message_room').val(), $('#room').val(), '', 'scroll')
   				}
   			}
   		}
   	})
   });
   
   function getUserMessage(toUserId, firstRoomId, room, reset, type)
   
   {
   
   	var initiateRoomId = '<?php echo $initiateRoomId ?>';
   
   	var initiateRoom = '<?php echo $initiateRoom ?>';
   
   	var uri = window.location.toString();
   	$('#current_date_message').hide();
   	if (initiateRoomId.length != 0 && uri.indexOf("?") > 0) {
   
   		console.log('innnn')
   
   		firstRoomId = initiateRoomId;
   
   	}
   	messageRoom = firstRoomId;
   	if ((window.location.href.indexOf('?') > -1) && (initiateRoom.length != 0) && (reset == 'reset')) {
   
   		window.location.href = window.location.pathname;
   
   	}
   	console.log('roomId:' + firstRoomId)
   	if (firstRoomId && type != 'scroll') {
   		$('#message_room').val(firstRoomId);
   		messagePage = 1;
   
   	}
   	if (messagePage == 1) {
   		$('#message_history_loader').show();
   	}
   	$('#room').val(room);
   
   	$('#to_user').val(toUserId)
   
   	$.ajax({
   
   		url: baseUrl + "chat/user-message",
   
   		data: {
   			user_id: $('#login_user').val(),
   			room_id: $('#message_room').val(),
   			page: messagePage
   		},
   
   		method: 'get',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   			$('#message_history_loader').hide();
   			// setTimeout(function() {
   			// 	$('#current_date_message').show()
   			// }, 10000)
   			$('#load_more_loader').hide();
   			$(".send_btn").css("pointer-events", "");
   			scroll = 'no';
   			$('.emoji-wysiwyg-editor').html('');
   			$('.write_msg').focus();
   			//$(".write_msg").attr("placeholder", "Type message here");
   			$('.write_msg').css({
   				'cursor': "pointer"
   			});
   			var uri = window.location.toString();
   
   
   			console.log(messageRoom)
   			if (messageRoom != $('#message_room').val()) {
   				messageRoom = $('#message_room').val();
   			}
   			if (uri.indexOf("?") > 0) {
   
   				console.log('reset url')
   
   				var clean_uri = uri.substring(0, uri.indexOf("?"));
   
   				window.history.replaceState({}, document.title, clean_uri);
   
   				'<?php echo $initiateRoomId = '' ?>';
   
   				'<?php echo $initiateRoom = '' ?>';
   
   				'<?php echo $toUser = '' ?>';
   
   				'<?php echo $chat_initiated = '' ?>';
   
   			}
   
   			if (result.data.rows.length > 0) {
   
   				messageTotalpage = result.data.totalPages;
   				if (result.data.rows[0].to_user == 0) {
   
   					$('.add-to-group').show();
   
   					$('#group_profile').show();
   					$('#group_edit').show()
   
   					$('#group_name').text(result.data.rows[0].room.group_name);
   
   					var toUserName = result.data.rows[0].room.group_name;
   
   					var toUserProfile = result.data.rows[0].room.profile;
   					$('#front_user_active').hide();
   					$('#profile_group_id').val(result.data.rows[0].room.id)
   				} else {
   
   					$('#group_edit').hide()
   					$('.add-to-group').hide();
   
   					$('#group_profile').hide();
   
   					$('#to_user_name').text()
   
   					var toUserName = ($('#login_user').val() == result.data.rows[0].to_user) ? result.data.rows[0].user.name : result.data.rows[0].toUser.name;
   
   					var toUserProfile = ($('#login_user').val() == result.data.rows[0].to_user) ? result.data.rows[0].user.profile : result.data.rows[0].toUser.profile;
   					var toUserOnlineStatus = ($('#login_user').val() == result.data.rows[0].to_user) ? result.data.rows[0].user.is_online : result.data.rows[0].toUser.is_online;
   					(toUserOnlineStatus == 'online') ? $('#front_user_active').show(): $('#front_user_active').hide();
   
   				}
   
   				$("#to_user_profile").attr("src", toUserProfile);
   
   				$("#to_user_user_name").html(toUserName);
   
   				$('.front-user-info').show();
   
   				console.log('userName:' + toUserName)
   
   				var page = result.data['currentPage'];
   
   				var totalPage = result.data['totalPages'];
   
   				var str = "";
   
   				if (page == 1) {
   
   					$('.msg_history').html('');
   
   				}
   				/*
   				if(result.data['currentPage'] < totalPage){
   
   					 page = parseInt(page)+1;
   
   					 var buttonStr = "<center class='load-button'><button type='button' onclick='getUserMessage("+toUserId+','+firstRoomId+','+'`'+room+'`'+','+"`unset`"+','+page+")' class='btn btn-primary'>loadmore</button></center>";
   
   				}*/
   				var buttonStr = "<center><span id='load_more_loader' style='display:none'><i class='fas fa-spinner fa-spin'></i></span></center>"
   
   
   
   				var allMessages = result.data.rows.reverse();
   
   
   
   				for (var i = 0; i < allMessages.length; i++) {
   
   					if (result.data.rows[i].to_user) {
   						var readUsers = JSON.parse("[" + result.data.rows[i].read_by + "]");
   						if (readUsers.includes(result.data.rows[i].to_user)) {
   							var messageRead = '';
   						} else {
   							var messageRead = 'none';
   						}
   					} else {
   						var messageRead = 'none';
   					}
   
   					if (result.data.rows[i].user_id != loginUserId) {
   
   
   						if (allMessages[i].files.length != 0) {
   
   							for (j = 0; j < allMessages[i].files.length; j++) {
   
   								if (allMessages[i].files[j].type == 'mp4') {
   
   									str = str + " <div class='incoming_msg' id='incoming_message_" + result.data.rows[i].id + "' >" +
   
   										"<div class='inn-message'><div class='ur-name'><span class='ur-img'><img src='" + result.data.rows[i].user.profile + "' alt='sunil'></span><strong>" + result.data.rows[i].user.name + "</strong></div>" +
   
   										"<div class='received_msg'>" +
   
   										"<div class='received_withd_msg'>" +
   
   										"<video width='300' height='200' controls>" +
   
   										"<source src='" + allMessages[i].files[j].fileUrl + "' type='video/mp4'>" +
   
   										"</video>" +
   
   										"</div>" +
   
   										"<span class='time_date'> " + result.data.rows[i].created_at + "</span></div></div></div>";
   
   								} else {
   
   									str = str + " <div class='incoming_msg' id='incoming_message_" + result.data.rows[i].id + "' >" +
   
   
   
   										"<div class='inn-message'><div class='ur-name'><span class='ur-img'><img src='" + result.data.rows[i].user.profile + "' alt='sunil'></span><strong>" + result.data.rows[i].user.name + "</strong></div>" +
   
   
   
   										"<div class='received_msg'>" +
   
   
   
   										"<div class='received_withd_msg'>" +
   
   
   
   										"<div class='received_al_msg'>" +
   
   
   
   										"<a target='_blank' href='" + allMessages[i].files[j].fileUrl + "' >" +
   
   										"<img src='" + allMessages[i].files[j].url + "'>" +
   
   										"</a></div>" +
   
   										"<span class='time_date'> " + result.data.rows[i].created_at + "</span></div></div></div></div>";
   
   								}
   
   
   
   							}
   
   						}
   
   
   
   						if (result.data.rows[i].message) {
   
   
   
   							str = str + " <div id='incoming_message_" + result.data.rows[i].id + "' class='incoming_msg'>" +
   
   
   
   								"<div class='inn-message'><div class='ur-name'><span class='ur-img'><img src='" + result.data.rows[i].user.profile + "' alt='sunil'></span><strong>" + result.data.rows[i].user.name + "</strong></div>" +
   
   
   
   								"<div class='received_msg'>" +
   
   
   
   								"<div class='received_withd_msg'>" +
   
   
   
   								"<p>" + result.data.rows[i].message + "</p>" +
   
   
   
   								"<span class='time_date'> " + result.data.rows[i].created_at + "</span></div></div></div></div>";
   
   
   
   						}
   						if (result.data.rows[i].to_user) {
   							socket.emit('message_receiving', {
   								message_id: result.data.rows[i].id,
   								message_room: $('#message_room').val()
   							})
   						}
   					} else {
   
   
   
   						if (allMessages[i].files.length != 0) {
   
   
   
   							for (j = 0; j < allMessages[i].files.length; j++) {
   
   								if (allMessages[i].files[j].type == 'mp4') {
   
   									str = str + "<div class='outgoing_msg' id='remove_message_" + result.data.rows[i].id + "'>" +
   
   										"<div class='sent_msg'><div class='dropdown'><button type='button' class='btn btn-primary' data-toggle='dropdown' id='dropdownMenuLink' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-v'></i></button><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><span><a class='btn' href='javascript:void(0)' onclick='deleteMessage(" + result.data.rows[i].id + ")'><i  class='fas fa-trash'></i> Delete</a></span></div></div>" +
   
   										"<div style=''>" +
   
   										"<video width='300' height='200' controls>" +
   
   										"<source src='" + allMessages[i].files[j].fileUrl + "' type='video/mp4'>" +
   
   										"</video>" +
   
   										"</div></br>" +
   
   										"<span class='time_date'>" + allMessages[i].created_at + "<i style='display:" + messageRead + "' class='fas fa-check-double' style='font-size:15px'></i></span> </div></div>";
   
   								} else {
   
   									str = str + "<div class='outgoing_msg' id='remove_message_" + result.data.rows[i].id + "' >" +
   
   
   
   										"<div class='sent_msg'><div class='dropdown'><button type='button' class='btn btn-primary' data-toggle='dropdown' id='dropdownMenuLink' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-v'></i></button><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><span><a class='btn' href='javascript:void(0)' onclick='deleteMessage(" + result.data.rows[i].id + ")'><i class='fas fa-trash'></i> Delete</a></span></div></div>" +
   
   
   
   										"<div class='received_al_msg'>" +
   
   										"<a target='_blank' href='" + allMessages[i].files[j].fileUrl + "'>" +
   
   										"<img src='" + allMessages[i].files[j].url + "'>" +
   
   										"</a></div>" +
   
   										"<span class='time_date'>" + result.data.rows[i].created_at + "<i style='display:" + messageRead + "' class='fas fa-check-double' style='font-size:15px'></i></span> </div></div>";
   
   								}
   
   
   
   							}
   
   
   
   						}
   
   
   
   						if (result.data.rows[i].message) {
   
   
   
   							str = str + "<div id='remove_message_" + result.data.rows[i].id + "' class='outgoing_msg'><div class='sent_msg'><div class='dropdown'><button type='button' class='btn btn-primary' data-toggle='dropdown' id='dropdownMenuLink' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-v'></i></button><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><span ><a class='btn' onclick='editMessage(" + result.data.rows[i].id + ")' href='javascript:void(0)'><i class='fas fa-pencil-alt'></i> Edit</a></span><span><a href='javascript:void(0)' onclick='deleteMessage(" + result.data.rows[i].id + ")' ><i  class='fas fa-trash'></i> Delete</a></span></div></div><p>" + result.data.rows[i].message + "</p>" +
   
   
   
   								"<span class='time_date'>" + result.data.rows[i].created_at + "<i style='display:" + messageRead + "'class='fas fa-check-double' style='font-size:15px'></i></span> </div></div>";
   
   
   
   						}
   
   						$('#last_sent_message_id').val(result.data.rows[i].id)
   
   					};
   
   
   
   				}
   
   				//if(result.data['currentPage']==1){
   
   				$('.msg_history').prepend(str);
   
   				//}else{
   
   				//  $('.msg_history').prepend(str);
   
   				//}
   
   				//$('.load-button').remove();
   
   				/*if(result.data['currentPage'] < result.data['totalPages']){*/
   
   				$('.msg_history').prepend(buttonStr);
   
   				//}
   
   				$('.chat_list').removeClass('active_chat');
   
   				$('#connect_row_' + firstRoomId).addClass('active_chat');
   
   				if (result.data['currentPage'] == 1) {
   
   					$(".msg_history").animate({
   						scrollTop: $('.msg_history')[0].scrollHeight
   					}, "slow");
   
   				} else {
   					$(".msg_history").animate({
   						scrollTop: 5
   					}, "slow");
   				}
   
   				$('#count_' + firstRoomId).hide();
   
   			}
   
   		}
   
   	});
   
   }
   
   
   
   var uploadedFiles = [];
   
   function readURL(input) {
   
   	var files = input.files;
   
   	var count = 0;
   
   	for (var i = 0; i < files.length; i++) {
   
   		uploadedFiles.push(files[i])
   
   		var reader = new FileReader();
   
   		var fileType = files[i].type;
   
   		console.log(files[i].type)
   
   		var fileSize = bytesToSize(files[i].size);
   
   		var fileName = files[i].name;
   
   		reader.readAsDataURL(files[i]);
   
   		reader.onload = function(e) {
   
   			if (fileType.includes("image")) {
   
   				var img = "<div class='image_preview_inn' id='file_" + count + "'  style='width:100px;height:100px;float:left'><img   src='" + e.target.result + "'/><a title='Delete'  href='javascript:void(0)' ><i onclick='removeFile(`" + count + "`)' class='fa fa-times'></i></a>" +
   
   					"</div>";
   
   			} else {
   
   				var img = "<div class='image_preview_inn' id='file_" + count + "' style='width:100px;height:100px;float:left'><img   src='" + BASEURL + "/node_chat//app/controllers/images/doc_icon.png'  /><a title='Delete' href='javascript:void(0)' ><i onclick='removeFile(`" + count + "`)' class='fa fa-times'></i></a>" +
   
   					"</div>";
   
   			}
   
   			console.log(i)
   
   			count++;
   
   			$('#image_preview').append(img);
   
   		}
   
   	}
   
   }
   
   
   
   function removeFile(index) {
   	/* if (index > -1) {
   
   	  uploadedFiles.splice(index, 1);
   
   	}*/
   
   	delete uploadedFiles[index];
   
   	$('#file_' + index).remove();
   
   }
   
   
   
   function bytesToSize(bytes) {
   
   	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   
   	if (bytes == 0) return '0 Byte';
   
   	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   
   	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
   }
   
   
   $("#images").change(function() {
   	readURL(this);
   
   });
   
   function editMessage(messageId) {
   	var message = $('#remove_message_' + messageId).find('p').html();
   	$('.emoji-wysiwyg-editor').html(message);
   	$('.emoji-wysiwyg-editor').focus();
   	$('.write_msg').scrollRight = $('.write_msg').scrollWidth;
   	$('#message_id').val(messageId);
   }
   
   function deleteMessage(messageId) {
   	bootbox.confirm({
   		message: "Are you sure you want delete?",
   		buttons: {
   			confirm: {
   				label: 'Yes',
   				className: 'delete-message'
   			},
   			cancel: {
   				label: 'No',
   				className: 'btn-danger'
   			}
   		},
   		callback: function(result) {
   			if (result) {
   				$.ajax({
   					url: baseUrl + "chat/delete-message",
   					data: {
   						message_id: messageId
   					},
   					method: 'get',
   					dataType: 'JSON',
   					success: function(result) {
   						$('#remove_message_' + messageId).remove();
   						socket.emit('delete_message', {
   							room_id: $('#message_room').val(),
   							user_id: $('#login_user').val(),
   							message_id: messageId
   						})
   					}
   				});
   			}
   		}
   	});
   }
   
   
   
   
   function sendMessage() {
   	var files = [];
   	var data = new FormData();
   	for (var i = 0; i < uploadedFiles.length; i++) {
   		data.append('files[]', uploadedFiles[i]);
   	}
   	var loginUserId = localStorage.getItem('userId');
   	var message = ($('.emoji-wysiwyg-editor').html()) ? $('.emoji-wysiwyg-editor').html() : $('#initiate_message').val();
   	if(message.indexOf("http://") == 0 || message.indexOf("https://") == 0){
   		message = message.replace(/<\/?[^>]+(>|$)/g, "");
   		message = message.replace(
   			/((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)/g,
   			'<a href="$1" target="_blank">$1</a>'
   		);
       }
   	if ((uploadedFiles.length == 0) && (!message)) {
   		console.log('innnn')
   		$('#message-error').show();
   	} else {
   		$(".send_btn").css("pointer-events", "none");
   		$('#message-error').hide();
   
   		var toUser = '<?php echo $toUser ?>'
   
   		if (toUser.length > 0) {
   
   			$('#to_user').val(toUser)
   
   		}
   		data.append('from_user', loginUserId);
   		data.append('room_id', $('#message_room').val());
   		data.append('message', message);
   		data.append('to_user', $('#to_user').val());
   		data.append('message_id', $('#message_id').val());
   		$.ajax({
   			url: baseUrl + "chat/send-message",
   			cache: false,
   			contentType: false,
   			enctype: 'multipart/form-data',
   			processData: false,
   			data: data,
   			method: 'post',
   			dataType: 'JSON',
   			success: function(result)
   			{
   				connectionPage = 1;
   				scroll = 'no';
   				$('#last_sent_message_id').val(result.data.id)
   				var roomName = $('#room').val();
   
   				var message = $('.write_msg').val();
   
   				if ($('#message_id').val() == '') {
   
   					socket.emit('send-chat-message', roomName, result.data)
   
   				} else {
   
   					socket.emit('update-message', result.data)
   
   				}
   
   				$('#message_emoji').val('')
   
   				$('#image_preview').html('');
   
   				$('.emoji-wysiwyg-editor').html('');
   
   				$('.search-bar').val('');
   
   				uploadedFiles = [];
   
   				var str = '';
   
   				if (result.data.message) {
   					str = str + "<div id='remove_message_" + result.data.id + "' class='outgoing_msg'><div class='sent_msg'><div class='dropdown'><button type='button' class='btn btn-primary' data-toggle='dropdown' id='dropdownMenuLink' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-v'></i></button><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><span><a class='btn' href='javascript:void(0)' onclick='editMessage(" + result.data.id + ")'><i  class='fas fa-pencil-alt'></i> Edit</a></span><span><a class='btn' href='javascript:void(0)' onclick='deleteMessage(" + result.data.id + ")'><i  class='fas fa-trash'></i> Delete</a></span></div></div><p>" + result.data.message + "</p>" +
   						"<span class='time_date'>" + result.data.created_at + "<i style='display:none'class='fas fa-check-double' style='font-size:15px'></i></span> </div></div>";
   				}
   				if (result.data.files.length != 0) {
   					for (j = 0; j < result.data.files.length; j++) {
   						if (result.data.files[j].type == 'mp4') {
   							str = str + "<div class='outgoing_msg'>" +
   								"<div class='sent_msg'><div class='dropdown'><button type='button' class='btn btn-primary' data-toggle='dropdown' id='dropdownMenuLink' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-v'></i></button><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><span><a class='btn' href='javascript:void(0)' onclick='deleteMessage(" + result.data.id + ")'><i  class='fas fa-trash'></i> Delete</a></span></div></div>" +
   								"<div style=''>" +
   								"<video width='300' height='200' controls>" +
   								"<source src='" + result.data.files[j].fileUrl + "' type='video/mp4'>" +
   								"</video>" +
   								"</div></br>" +
   								"<span class='time_date'>" + result.data.created_at + "<i style='display:none'class='fas fa-check-double' style='font-size:15px'></i></span> </div></div>";
   						} else {
   							str = str + "<div class='outgoing_msg' id='remove_message_" + result.data.id + "' >" +
   								"<div class='sent_msg'><div class='dropdown'><button type='button' class='btn btn-primary' data-toggle='dropdown' id='dropdownMenuLink' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-v'></i></button><div class='dropdown-menu' aria-labelledby='dropdownMenuLink'><span><a class='btn' href='javascript:void(0)' onclick='deleteMessage(" + result.data.id + ")'><i  class='fas fa-trash'></i> Delete</a></span></div></div>" +
   								"<div class='received_al_msg'>" +
   								"<a target='_blank' href='" + result.data.files[j].fileUrl + "'>" +
   								"<img src='" + result.data.files[j].url + "'>" +
   								"</a></div>" +
   								"<span class='time_date'>" + result.data.created_at + "</span> </div></div>";
   						}
   					}
   				}
   				if($('#message_id').val()==''){
   				getConnectedUserList(loginUserId);
   				}
   				if ($('#message_id').val() == '') {
   					$('.msg_history').append(str);
   				} else {
   					$('#remove_message_' + $('#message_id').val()).find('p').html(result.data.message);
   					$('#message_id').val('');
   				}
   				var pTop = $('.msg_history').scrollTop();
   				$(".msg_history").animate({
   					scrollTop: $('.msg_history')[0].scrollHeight
   				}, "slow");
   				$(".send_btn").css("pointer-events", "");
   				console.log('Message id:'+$('#message_id').val())
   				
   				setTimeout(function() {
   					$('#count_' + firstRoomId).hide();
   
   				}, 1000)
   			}
   		});
   
   	}
   
   }
   
   
   
   function tellForTyping() {
   
   	socket.emit('is_typing', $('#message_room').val(), localStorage.getItem('loginUserName'), loginUserId)
   
   }
   
   
   
   socket.on('online', data => {
   
   	$('#active' + data.user_id).show();
   
   });
   
   socket.on('offline', data => {
   $('#active' + data.user_id).hide();

   });
   
   
   socket.on('typing', data => {
   
   	if (myRooms.includes(parseInt(data.room)) && (parseInt(data.user_id) != loginUserId) && ($('#message_room').val() == data.room)) {
   
   		$('#is_typing').show();
   
   		$('#is_typing').text(data.name + '  is typing ......')
   
   		setTimeout(function() {
   
   			$('#is_typing').hide();
   
   		}, 1000)
   
   	}
   
   	if (myRooms.includes(parseInt(data.room)) && (parseInt(data.user_id) != loginUserId) && ($('#message_room').val() != data.room)) {
   
   		$('#is_typing' + data.room).show();
   
   		$('#is_typing' + data.room).text(data.name + '  is typing ......')
   
   		setTimeout(function() {
   
   			$('#is_typing' + data.room).hide();
   
   		}, 500)
   
   	}
   
   })
   
   
   
   socket.on('chat-message', data => {
   
   	getAllRooms(loginUserId)
   
   	if (myRooms.includes(data.message.room_id) && (data.message.user_id != loginUserId)) {
   
   		var str = '';
   
   		if (data.message.message) {
   
   
   
   			str = str + " <div id='incoming_message_" + data.message.id + "' class='incoming_msg'>" +
   
   
   
   				"<div class='inn-message'><div class='ur-name'><span class='ur-img'><img src='" + data.message.user.profile + "' alt='sunil'></span><strong>" + data.message.user.name + "</strong></div>" +
   
   
   
   				"<div class='received_msg'>" +
   
   
   
   				"<div class='received_withd_msg'>" +
   
   
   
   				"<p>" + data.message.message + "</p>" +
   
   
   
   				"<span class='time_date'> " + data.message.created_at + "</span></div></div></div></div>";
   
   
   
   		}
   
   		if (data.message.files.length != 0) {
   
   			for (j = 0; j < data.message.files.length; j++) {
   
   				if (data.message.files[j].type == 'mp4') {
   
   					str = str + " <div class='incoming_msg' id='incoming_message_" + data.message.id + "' >" +
   
   						"<div class='inn-message'><div class='ur-name'><span class='ur-img'><img src='" + data.message.user.profile + "' alt='sunil'></span><strong>" + data.message.user.name + "</strong></div>" +
   
   						"<div class='received_msg'>" +
   
   						"<div class='received_withd_msg'>" +
   
   						"<video width='300' height='200' controls>" +
   
   						"<source src='" + data.message.files[j].fileUrl + "' type='video/mp4'>" +
   
   						"</video>" +
   
   						"</div>" +
   
   						"<span class='time_date'> " + data.message.created_at + "</span></div></div></div>";
   
   				} else {
   
   					str = str + " <div class='incoming_msg' id='incoming_message_" + data.message.id + "' >" +
   
   
   
   						"<div class='inn-message'><div class='ur-name'><span class='ur-img'><img src='" + data.message.user.profile + "' alt='sunil'></span><strong>" + data.message.user.name + "</strong></div>" +
   
   						"<div class='received_msg'>" +
   
   						"<div class='received_withd_msg'>" +
   
   						"<div class='received_al_msg'>" +
   
   						"<a target='_blank' href='" + data.message.files[j].fileUrl + "' >" +
   
   						"<img src='" + data.message.files[j].url + "'>" +
   
   						"</a></div>" +
   
   						"<span class='time_date'> " + data.message.created_at + "</span></div></div></div></div>";
   
   				}
   
   			}
   
   		}
   		if ($('#message_room').val() == data.message.room_id) {
   			$('.msg_history').append(str);
   		}
   
   		$(".msg_history").animate({
   			scrollTop: $('.msg_history')[0].scrollHeight
   		}, "slow");
   		connectionPage = 1;
   		if ('<?php echo $toUser ?>' && '<?php echo $chat_initiated ?>' == 'no') {
   		getConnectedUserList(loginUserId)
   		}
   
   		if (data.message.room_id == $('#message_room').val() && data.message.to_user != 0) {
   
   			readMessage(data.message.id)
   
   			setTimeout(function() {
   
   				if (firstRoomId == data.message.room_id) {
   
   					$('#count_' + firstRoomId).hide();
   
   				}
   
   			}, 2000)
   
   		}
   
   
   
   	}
   
   })
   
   
   
   function readMessage(messageId) {
   
   	$.ajax({
   
   		url: baseUrl + "chat/read-message",
   
   		data: {
   			message_id: messageId,
   			user_id: $('#login_user').val()
   		},
   
   		method: 'get',
   
   		dataType: 'JSON',
   
   		success: function(result)
   
   		{
   
   			socket.emit('message_receiving', {
   				message_id: messageId,
   				message_room: $('#message_room').val()
   			})
   
   		}
   
   	});
   
   }
   
   socket.on('message_received', data => {
   	if ($('#message_room').val() == data.message_room) {
   		$('#remove_message_' + data.message_id + ' .time_date').find('i').css({
   			'display': ''
   		});
   	}
   
   })
   
   socket.on('updated-message', data => {
   
   	getAllRooms(loginUserId)
   
   	if (myRooms.includes(data.room_id) && (data.user_id != loginUserId)) {
   
   		$('#incoming_message_' + data.id + ' .received_withd_msg').find('p').html(data.message);
   
   		$(".msg_history").animate({
   			scrollTop: $('.msg_history')[0].scrollHeight
   		}, "slow");
   
   	}
   
   })
   
   
   
   socket.on('remove-message', data => {
   
   	getAllRooms(loginUserId)
   
   	if (myRooms.includes(parseInt(data.room_id)) && (parseInt(data.user_id) != loginUserId)) {
   
   		$('#incoming_message_' + data.message_id).remove();
   
   	}
   
   })
   
   
   
   socket.on('removed-from-group', data => {
   
   	getAllRooms(loginUserId)
   
   	console.log(data)
   
   	console.log(myRooms)
   
   	if (!myRooms.includes(parseInt(data.room_id)) && (parseInt(data.user_id) != loginUserId)) {
   
   		getConnectedUserList(loginUserId)
   
   	}
   
   })
   
   
   
   socket.on('added-into-group', data => {
   
   	getAllRooms(loginUserId)
   
   	if (myRooms.includes(parseInt(data.room_id)) && (parseInt(data.user_id) != loginUserId)) {
   
   		getConnectedUserList(loginUserId)
   
   	}
   
   })
   
   
   
   
   
   /*
   
   $(function() {
   
      var oTop = $('body').offset().top - window.innerHeight;
   
   console.log('historyHeight:'+$('.msg_history').offset().top)
   
      $('.msg_history').scroll(function(){
   
   
   
   		var pTop = $('.msg_history').scrollTop();
   
   		console.log( pTop + ' - ' + oTop );   //just for your debugging
   
   		if( pTop == 0 ){
   
   			start_count();
   
   		}
   
   	});
   
   });
   
   */
   
   function start_count() {
   
   	console.log('start_count');
   
   	//Add your code here
   
   }
</script>
<script>
   $(function() {
   
   	// Initializes and creates emoji set from sprite sheet
   
   	window.emojiPicker = new EmojiPicker({
   
   		emojiable_selector: '[data-emojiable=true]',
   
   		assetsPath: './lib/img/',
   
   		popupButtonClasses: 'far fa-smile'
   
   	});
   
   	// Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
   
   	// You may want to delay this step if you have dynamically created input fields that appear later in the loading process
   
   	// It can be called as many times as necessary; previously converted input fields will not be converted again
   
   	window.emojiPicker.discover();
   
   });
</script>
<script>
   // Google Analytics
   
   (function(i, s, o, g, r, a, m) {
   	i['GoogleAnalyticsObject'] = r;
   	i[r] = i[r] || function() {
   
   		(i[r].q = i[r].q || []).push(arguments)
   	}, i[r].l = 1 * new Date();
   	a = s.createElement(o),
   
   		m = s.getElementsByTagName(o)[0];
   	a.async = 1;
   	a.src = g;
   	m.parentNode.insertBefore(a, m)
   
   })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
   
   
   
   ga('create', 'UA-49610253-3', 'auto');
   
   ga('send', 'pageview');
</script>
<script src="./lib/js/config.js"></script>
<script src="./lib/js/util.js"></script>
<script src="./lib/js/jquery.emojiarea.js"></script>
<script src="./lib/js/emoji-picker.js"></script>
<?php
   include("./common-page/footer.php");
   
   
   
   ?>