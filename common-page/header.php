
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>Socket Chat</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" />
	<link rel="stylesheet" href="./css/toastr.css">
    <link href="./lib/css/emoji.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">

    <script src="./js/jquery.min.js"></script>
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/toastr.js"></script>

   <script src="./js/bootbox.min.js"></script>
    <script src="http://localhost:8080/socket.io/socket.io.js"> </script>
    <script>
        const socket = io('http://localhost:8080/')
        const baseUrl = "http://localhost:8080/api/";
        var BASEURL = "http://localhost/nodeChat";
    </script>

</head>
<!--Coded with love by Mutiullah Samim-->
<body>
<script>
	if(!localStorage.getItem('userId')){
			window.location = "<?php echo 'index.php' ?>"
	}
</script>
<div class="main-chat-page">

    <div class="container">

        <div class="main-chat-page-inn">

            <div class='chat-header'>

                <div id="profile_div">

                    <div class="row">

                        <div class="col-lg-9">

                            <div class="chat_people">
								
                                <div class="peopel_image" id="profile_image"> 
                                    <img style="height:60px;width:75px" id="user_profile" src="" alt=""> 

                                </div>
								<a onclick="editModel('private')" class='edit-profile' href="javascript:void(0)"><i class="fas fa-pencil-alt"></i></a>
                                <div class="chat_ib" id="user_name"></div>

                            </div>

                        </div>
                        <div class="col-lg-3">
                            <div class="head-right text-right">
                                <div class="dropdown dropleft float-right">
                                <button type="button" class="btn btn-primary" data-toggle="dropdown">
                                    <i class="fas fa-ellipsis-v"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <form>
                                        <ul>
                                            <li><button type="button" class="btn btn-link batn-logout">Logout</button></li>
                                        </ul>
                                    </form>
								</div>
								</div>
							</div>
						</div>
					</div>

                    

                </div>

                <div class="right-top-header">

                    <h3>FrontUser</h3>

                    <div class="">

                        <div class="peopel_image" id=""> 

                            <img  style="height:60px;width:75px" src="http://localhost/nodeChat/node_chat//app/controllers/images/download (6).jpg" alt=""/> 

                        </div>

                    </div>

                </div>

            </div>
			
<div class="modal" id="updateProfileModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Edit profile</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form id="edit_profile">
		  <div class="modal-body mx-3">
			  <input type='hidden' name='group_id' value='' id='profile_group_id'/>
			  <input type='hidden' name='type' value='' id='profile_type'/>
			<div class="md-form mb-5">
			  <i class="fas fa-group prefix grey-text"></i>
			  <input type="text" placeholder="Enter Profile Name" name="name" id="profile_name" class="form-control  require">
			  <span id='error_profile_name' style='display:none' class="alert-danger" >The field is required.</span>
			</div>
			<div class="chat_people">
			<div class="peopel_image"> 
              <img style="height:60px;width:75px" id="edit_profile_image" src="" alt=""> 

            </div>
			</div>
			<div class="md-form mb-5">
			  <i class="fas fa-group prefix grey-text"></i>
			  <input type="file"  name="profile" id="profile" class="form-control  require" accept="image/*">
			  <span id='error_profile_image' style='display:none' class="alert-danger" >The field is required.</span>
			</div>
		  </div>
	  </form>
      <div class="modal-footer d-flex justify-content-center">
     <button class="btn btn-indigo" onclick="updateProfile()">Update <i style='display:none' id='btn-update' class='fas fa-spinner fa-spin'></i></button>
    </div>
    </div>
  </div>
</div>

    



<script>

 var universalSocket;



     toastr.options = {

            "closeButton": true,

            "debug": false,

            "newestOnTop": false,

            "progressBar": true,

            "preventDuplicates": true,

            "onclick": null,

            "showDuration": "100",

            "hideDuration": "1000",

            "timeOut": "5000",

            "extendedTimeOut": "1000",

            "showEasing": "swing",

            "hideEasing": "linear",

            "showMethod": "show",

            "hideMethod": "hide"

        };

		

	$('.batn-logout').on('click',function(){
		var logOutId = localStorage.getItem('userId');
		 $.ajax({

            url:baseUrl+"auth/logout" ,

			data:{email :localStorage.getItem('email')},

            method: 'post',

            dataType: 'JSON',

            success:function(result)
            {

				if(result.success){
					socket.emit('is_offline', logOutId)
					localStorage.clear();

					toastr.success('Logout successfully.');

					setTimeout(function(){

					  window.location = "<?php echo 'index.php' ?>"	

					},1000)

				}else{

					toastr.error(result.message);

				}

		    }

        });

	});
	
	function readProfileURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
		  $('#edit_profile_image').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]); 
	  }
	}

	$("#profile").change(function() {
	  readProfileURL(this);
	});

	function editModel(type){
		$('#edit_profile')[0].reset();
		//$('.edit-profile').html("<i class='fas fa-spinner fa-spin'></i>");
		if(type == 'private'){
		$.ajax({
				url:baseUrl+"chat/user-detail" ,
				data:{user_id:localStorage.getItem('userId')},
				method: 'get',
				dataType: 'JSON',
				success:function(result)
				{
					if(result.success){
					  $('#profile_type').val(type)
					  $('#edit_profile_image').attr('src',result.data.profile)
					  //$('.edit-profile').html("<i class='fas fa-pencil-alt'></i>");
					  $('#profile_name').val(result.data.name);
					  $('#updateProfileModel').show();
					}
				}
			});
		}else{
			//alert($('#to_user_user_name').val())
			$('#profile_type').val(type)
			$('#profile_name').val($('#to_user_user_name').text());
			$('#edit_profile_image').attr('src',$('#to_user_profile').attr('src'))
			$('#updateProfileModel').show();
		}
		
	}
	
	$('.close').on('click',function(){
		$('#updateProfileModel').hide();
	});
	
	function updateProfile(){
		var flage =true;
		if($('#profile_name').val()==''){
			$('#error_profile_name').show();
			flage = false;
		}
		if(flage){
			$('.btn').attr('disabled',true);
			$('#btn-update').show()
			var data = new FormData();
			data.append('user_id',($('#profile_type').val()=='private')?localStorage.getItem('userId'):$('#profile_group_id').val());
			data.append('type', $('#profile_type').val());
			data.append('name',$('#profile_name').val());
			data.append('profile',$('#profile')[0].files[0]);
			$.ajax({
					url:baseUrl+"auth/update-profile" ,
					cache: false,
					contentType: false,
					enctype: 'multipart/form-data',
					processData: false,
					data:data,
					method: 'post',
					dataType: 'JSON',
					success:function(result)
					{
						$('.btn').attr('disabled',false);
						if(result.success){
							$('#btn-update').hide()
							$('#updateProfileModel').hide();
							if($('#profile_type').val()=='private'){
								$('#user_profile').attr("src",result.data.profile);
								$('#user_name').html('<h5 >'+result.data.name+'</h5>')
								localStorage.setItem('loginUserName', result.data.name);
								localStorage.setItem('email', result.data.email);
								localStorage.setItem('loginUserProfile', result.data.profile);
							}else{
								var roomId = $('#profile_group_id').val();
								$("#connect_row_"+ roomId).find('.chat_img').find('img').attr("src",result.data.profile);
								$('#to_user_profile').attr('src',result.data.profile);
								$("#connect_row_"+ roomId).find('.chat_ib').find('h5').text(result.data.group_name);
								$('#to_user_user_name').text(result.data.group_name);
							}
						}else{
							toastr.error(result.message)
						}
						
					}
				});
		}
	}
</script>
