<?php 

include("./common-page/header.php");

?>

			<div class="messaging">
				<div class="inbox_msg user-list-page-main">
					<div class="inbox_people">
						<div class="chat-page-main-mesgs">
							<div class="row">
								<div class="col-sm-12">
									<div class="headind_srch">
										<div class="row">
											<div class="col-sm-4">
												<div class="recent_heading">
													<h3><i class="fas fa-users"></i> <small>User List</small></h3>
													<h3><a href="chat-page.php"><i class="fas fa-comments"></i> <small>Conversation List</small></a></h3>
													<h3>
														<button type="button" value='' onclick="openModel()">
															<i class="fas fa-user-plus"></i><small>Creat Group</small>
														</button> 
													</h3>
												</div>
											</div>
											<div class="col-sm-8">
												<div class="srch_bar text-right">
													<div class="stylish-input-group">
														<input type="text" class="search-bar" onkeyup="getUserList('search')"  placeholder="Search Username" >
														<span class="input-group-addon">
															<button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="inbox_chat">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<input  type="hidden" id="message_room" value="">
				<input  type="hidden" id="room" value="">
				<input  type="hidden" id="to_user" value="">
				<input  type="hidden" id="login_user_id" value="">
			</div>

			<center>
				<div class="loader" >
					<span id="loader" style='display:none'>
						<i class="fa-3x fas fa-spinner fa-spin"></i>
					</span>
				</div>
			</center>
		      
		</div>

    </div>



</div>

	

<script>



	



	var loginUserId;
	var page=1;
	var totalpage;
	var currentPage;

	$(document).ready(function(){
		$('.right-top-header').css({'display':'none'})
		$('#profile_div').css({'width':'100%'})
	    loginUserId = localStorage.getItem('userId');
		$('#login_user_id').val(loginUserId);
		getUserList();
		var profile = localStorage.getItem('loginUserProfile');
	    var name = localStorage.getItem('loginUserName');
	});
	
	var scroll = 'no';
	jQuery(function($) {
		$('.inbox_chat').on('scroll', function() {
			if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
				scroll = 'yes';
				//if(page!=1 && page<=totalpage){
				 getUserList()
				//}
			}
		})
	});

	
		

  function getUserList(type)
  {
	  
	   if(page <= totalpage){
        $('#loader').show();
	   }
	   if($('.search-bar').val() != ''){
		   page =1;
		   scroll == 'no';
	   }

	   if($('.search-bar').val() == '' && (type=='search')){
		   page =1;
	   }
	   console.log()
	   $.ajax({
            url:baseUrl+"chat/all-user-list" ,
			data:{user_id :$('#login_user_id').val(),search:$('.search-bar').val(),page:page},
            method: 'get',
            dataType: 'JSON',
            success:function(result)
            {
				$('#loader').hide();
				if(result.data.rows.length > 0){
				totalpage = result.data.totalPages;
				inbox_chat = currentPage;
				if(page==1|| type=='search'){
				 $('.inbox_chat').html('');
				}
				if(scroll == 'yes'){
				   page++;
				}
                
				var str = '';
				for (var i=0;i<result.data.rows.length; i++){
					str = str + " <div  class='chat_list'>"+
						   "<a href='javascript:void(0)' onclick = 'initiateChat("+result.data.rows[i].id+")'>  "+
							 "<div class='chat_people'>"+
							 "<div class='chat_img'> <img src='"+result.data.rows[i].profile+"' alt='sunil'> </div>"+
							 "<div class='chat_ib'><div class='chat_comment'><i class='far fa-comment' aria-hidden='true'></i></div>"+
							 "<div class='chat_dtls'><h5>"+result.data.rows[i].name+"<span class='chat_date'> "+result.data.rows[i].created_at+"</span></h5>"+
							 "<p>"+result.data.rows[i].email+"</p></div>"+
							"</div></div></a></div>";
				}
				$('.inbox_chat').append(str);
				//$(".inbox_chat").animate({ scrollTop: $('.inbox_chat')[0].scrollHeight+10  }, "slow")
				}else if(page == 1){
					$('#loader').hide();
					$('.inbox_chat').html('<center><h5 id="no_record">No User Found.</h5></center>');

				}
            }
        });
	}

	

	function initiateChat(toUserId){

		var users = [];

		users.push(toUserId)

		users.push(loginUserId)

		console.log(users)

		$.ajax({

            url:baseUrl+"chat/create-room" ,

			data:{"userId" : JSON.stringify(users),"type":"private"},

            method: 'post',

            dataType: 'JSON',

            success:function(result)

            {

			  window.location =  'chat-page.php?room_id='+result.data.id+'&room='+result.data.room_id+'&to_user='+toUserId+'&chat_initiated='+result.data.chat_initiated

			}

            

        })

	}

	

</script>

 

<?php 



include("./common-page/footer.php");



?>